module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'rolling-blue': '#09d', // example use: class="bg-rolling-blue" or class="text-rolling-blue"
        'rolling-dark-blue': '#0b2332',
        // Custom colors can go here, then run `npm run build`
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
