# Code challenge for MW

The goal of this assignment is to introduce you to our typical workflow and see how you work.

## Dependencies & Documentation

1. [Git](https://git-scm.com/)
2. [Node / npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
3. [Tailwind CSS](https://tailwindcss.com/docs) documentation will be very useful.

## Getting up and running

1. Clone this repository `git clone git@bitbucket.org:nurelmdevelopers/mw-ui-challenge.git`
2. Create a feature branch such as `feature/code-challenge` off of the `develop` branch for your work.
3. From the mw-ui-challenge folder, run `npm install`
4. Now, you should be able to watch for changes by running with Live Server by running `npm run live-server`
  - The page should open at http://127.0.0.1:8080/ and you should see the started UI displayed in the browser
  - If you make any edits to tailwind.config.js (custom colors, breakpoints classess, etc), you'll need to build those by separately by stopping live server and then running `npm run build`, then revisit the start of #4 here to continue.
    - Note that we put Tailwind into development mode, so all tailwind.css styles should be available from the documentaion.
    - Also note that Live Server is optional here, and because we're using HTML, you should be able to simply open the index.html file in the browser and reload the page, too.
5. Make changes to `build/index.html` and/or `build/custom.css` (for custom styles)
6. Once complete, submit a Pull Request from Bitbucket to merge your feature into the `develop` git branch.

## Task

The project started might look familiar and comes from your portfolio [Rolling Beans - Webflow prototype](https://rolling-beans-cafe-deeaf0ec5b6c860a69d9.webflow.io/). We want to see how you might work on building an interface from scratch, since every project we do requires this.

In a lower fidelity, build out the rest of the started UI from your prototype using Tailwind CSS classes and/or custom CSS, targeting mobile -> large screens. We develop mobile first and would love to see that approach from you, this [Tailwind doc](https://tailwindcss.com/docs/responsive-design#mobile-first) is a good reference. You can see that we started it off using [flexbox](https://tailwindcss.com/docs/flex) primarily for the layout (rather than [grid](https://tailwindcss.com/docs/grid-template-columns)), but feel free to change this or use both. Your fidelity can be a little higher if you choose. We threw in a couple of the colors into tailwind.config.js and showed an example of how to use it in the index.html file.

Remember, we’re not looking for a finished product here. We’re more interested in how you work, solve problems and think about things when developing

## Bonus 
1. Consider increasing the fidelity for the header and navigation especially to match your mockups, including use of images and icons there.
2. Since we created this challenge, it looks like a fixed bottom navigation for mobile has been implemented, or we missed it. Tailwind CSS has all the classes needed to accomplish this behavior with just a few classes and a mobile first approach. Do try to implement this if you have time.
3. Performance is important and often a front end developer's responsibility. This repository really isn't production ready; see if you can identify why and let us know in your pull request what you think should be different.
