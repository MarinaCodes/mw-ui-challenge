// We switch to production to build only the tailwind classes we use.
// process.env.NODE_ENV = 'production';
process.env.NODE_ENV = 'development';

const purgecss = require('@fullhuman/postcss-purgecss')({
  content: [
    './build/*.html',
  ],
  defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
})

module.exports = {
  plugins: [
      require('tailwindcss'),
      require('autoprefixer'),
      ...process.env.NODE_ENV === 'production' ? [purgecss] : []
  ]
}